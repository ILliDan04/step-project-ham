function loadingAnimation(btn, time) {
    const loadingElem = $("<div></div>");
    loadingElem.addClass("loading-block");

    const loader = $("<div></div>");
    loader.addClass("loader");

    loadingElem.prepend(loader);
    $(btn).before(loadingElem);

    loader.animate({width: "100%"}, time / 2);

    setTimeout(() => {
        loadingElem.animate({opacity: "0"}, time / 2);
    }, time / 2);

    setTimeout(() => {
        loadingElem.remove();
    }, time);
}

// SECTION 2 TABS

$(".section2-menu__item").on("click", event => {
    $(".section2-menu__item").removeClass("section2-menu__active");
    $(event.target).addClass("section2-menu__active");
    const index = [...$(".section2-menu__item")].indexOf(event.target);

    $(".section2-info-block__data").removeClass("section2-visible");
    $($(".section2-info-block__data")[index]).addClass("section2-visible");
});

// SECTION 4 TABS

let targetType = "all";

$(".section4-nav__link").on("click", event => {
    event.preventDefault();

    $(".section4-nav__link").removeClass("section4-nav__active");
    $(event.target).addClass("section4-nav__active");

    targetType = $(event.target).attr("data-target");

    if (targetType === "all") {
        $(".section4-grid__element").removeClass("closed");
    } else {
        $(".section4-grid__element").addClass("closed");
        $(`.section4-grid__element[data-type="${targetType}"]`).removeClass("closed");
    }
});

$("#section4Btn").on("click", event => {
    const hiddenElements = $(".section4-grid__element.hidden");
    loadingAnimation(event.target, 2000);
    setTimeout(() => {
        if (hiddenElements.length === 12) {
            $(event.target).remove();
        }
    
        const firstHiddenElements = $.grep(hiddenElements, (item, index) => {
            if (index < 12) {
                return true;
            }
        });
    
        $(firstHiddenElements).removeClass("hidden");
    
        if (targetType !== "all") {
            $(".section4-grid__element").addClass("closed");
            $(`.section4-grid__element[data-type="${targetType}"]`).removeClass("closed");
        }
    }, 2000);

});

// SECTION 6 SLIDER

let indexOfSlide = 1;
const slideStep = $(".slide-content").width();
const slidePages = $(".slide-page");
const faceImages = $(".face-img");

const transitionTime = 300;

$("#slidePrev").on("click", () => {
    
    indexOfSlide--;
    faceImages.removeClass("face-active");
    $(faceImages[indexOfSlide - 1]).addClass("face-active");
    
    $(".slide-content").css("transition", `${transitionTime}ms`);
    $(".slide-content").css("transform", `translateX(${-indexOfSlide * slideStep}px)`);
    
    if (indexOfSlide === 0) {
        indexOfSlide = slidePages.length - 2;
        $(faceImages[indexOfSlide - 1]).addClass("face-active");
        setTimeout(() => {
            $(".slide-content").css("transform", `translateX(${-indexOfSlide * slideStep}px)`);
            $(".slide-content").css("transition", "none");
        }, transitionTime);
    }
});

$("#slideNext").on("click", () => {
    
    indexOfSlide++;
    faceImages.removeClass("face-active");
    $(faceImages[indexOfSlide - 1]).addClass("face-active");
    
    $(".slide-content").css("transition", `${transitionTime}ms`);
    $(".slide-content").css("transform", `translateX(${- indexOfSlide * slideStep}px)`);
    
    if (indexOfSlide === slidePages.length - 1) {
        indexOfSlide = 1;
        $(faceImages[indexOfSlide - 1]).addClass("face-active");
        setTimeout(() => {
            $(".slide-content").css("transform", `translateX(${-indexOfSlide * slideStep}px)`);
            $(".slide-content").css("transition", "none");
        }, transitionTime);
    }
});

faceImages.on("click", event => {
    $(".slide-content").css("transition", `${transitionTime}ms`);
    const indexOfFace = [...faceImages].indexOf(event.target) + 1;
    faceImages.removeClass("face-active");
    $(event.target).addClass("face-active");

    $(".slide-content").css("transform", `translateX(${-indexOfFace * slideStep}px)`);
    indexOfSlide = indexOfFace;
});


// SECTION 7 GRID

const mainGrid = $(".section7-grid__container").masonry({
    itemSelector: ".section7-grid__element-6",
    columnWidth: 390,
    gutter: 10,
});
    
const twoColGrid = $(".section7-grid__element-6-2-cols").masonry({
    itemSelector: ".section7-grid__element-3",
    columnWidth: 190,
    gutter: 10,
});

$(".section7-grid__element-6-2-cols").css("position", "absolute");

const threeColGrid = $(".section7-grid__element-6-3-cols").masonry({
    itemSelector: ".section7-grid__element-2",
    columnWidth: 125,
    gutter: 7.5,
})

$(".section7-grid__element-6-3-cols").css("position", "absolute");

mainGrid.imagesLoaded().done(() => {
    threeColGrid.masonry("layout");
    twoColGrid.masonry("layout");
    mainGrid.masonry("layout");
});

// Section 7 load mpre btn

$("#section7Btn").on("click", event => {
    loadingAnimation(event.target, 2000);
    setTimeout(() => {
        $(".section7-grid__element-6.hidden").removeClass("hidden");
        mainGrid.masonry("layout");
    }, 2000);

    $(event.target).remove();
})